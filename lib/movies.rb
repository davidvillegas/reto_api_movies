require_relative '../db/connection'
require_relative 'movies/actor'
require_relative 'movies/director'
require_relative 'movies/genre'
require_relative 'movies/language'
require_relative 'movies/movie'
require_relative 'movies/production'

module Tester

end	