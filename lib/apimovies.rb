require 'rest-client'
require 'json'

class GetMovie
		attr_accessor :url, :data, :apikey, :title

	def initialize
		#@title = gets.chomp
		@title = "the lord of the rings"
		@apikey = 'c8e51eb5'
		@token = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6ImY4OTE2ZjE5LTA4YWMtNDJhNS05ZGZiLTZlOTNlYzg5Y2EzNyIsImlhdCI6MTU0Njk5NjQ5OCwic3ViIjoiZGV2ZWxvcGVyLzgxNDY2ZTMzLTU0MzQtODk2MS00YTA3LWE1YzRjNGY3NmJlYyIsInNjb3BlcyI6WyJjbGFzaCJdLCJsaW1pdHMiOlt7InRpZXIiOiJkZXZlbG9wZXIvc2lsdmVyIiwidHlwZSI6InRocm90dGxpbmcifSx7ImNpZHJzIjpbIjIwMS4yMTAuNTYuMTIiLCIyMDEuMjEwLjU2LjI1NCJdLCJ0eXBlIjoiY2xpZW50In1dfQ.BBGjrHBTR75W92B5rCCj5Bi9ukbDtXhwAXu6h2wKjpyTJYvn8Z6w-rxWqVeRvMNJnKTdcQwxNcoohMx8M9NTVA'
		@tag = "PPV8CQQ2"
		#@url = "http://www.omdbapi.com/?t=terminator&apikey=#{@apikey}"
		#@url = "http://www.omdbapi.com/?t=#{@title}&apikey=#{@apikey}"
		@url = "https://api.clashofclans.com/v1/clans/%23#{@tag}"
	end
  
	# def bearer_token
	#   pattern = /^Bearer /
	#   header  = request.set_headers['Authorization']
	#   header.gsub(pattern, '') if header && header.match(pattern)
	# end

	def get_data
		RestClient.get(@url, {:Authorization => @token})
	end

	def parse_data
		JSON.parse get_data
	end

	def show_data
		@data = parse_data
		@data.each do |k| 
			puts k
		end	
	end

	def find_movie
		# bearer_token
		show_data		
	end

end

#print "Ingrese Película a Buscar: "
GetMovie.new.find_movie
#p GetMovie.new.parse_data