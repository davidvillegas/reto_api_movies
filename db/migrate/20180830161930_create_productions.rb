class CreateProductions < ActiveRecord::Migration[5.2]
  def change
    create_table :productions do |t|
      t.string :name
      t.references :movie, foreign_key: true
    end
  end
end
