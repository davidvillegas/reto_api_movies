class CreateLanguages < ActiveRecord::Migration[5.2]
  def change
    create_table :languages do |t|
      t.string :language
      t.references :movie_languages, foreign_key: true
    end
  end
end
